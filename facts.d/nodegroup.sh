#!/bin/sh

#
# This script returns the FQDN with trailing numbers removed from the hostname.
# This is useful for defining Hiera variables for server farms.
#
# eg.      NODES = node1.domain, node2.domain
#      NODEGROUP = node.domain
#

NODEGROUP=`/bin/hostname | sed 's/[0-9]*$//'`
DOMAIN=`/bin/hostname -d`

echo "nodegroup=${NODEGROUP}.${DOMAIN}"
