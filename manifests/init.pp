class linuxserver (
    $pelab_file_content = $linuxserver::params::pelab_file_content,
) {

    include dnsclient
    include ntp
    include linuxserver::profiles::pelab

}