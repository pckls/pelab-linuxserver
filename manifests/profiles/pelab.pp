class linuxserver::profiles::pelab {

    file { '/root/test.txt':
        content => template('linuxserver/pelab_file.erb'),
        owner   => root,
        group   => root,
        mode    => 0600,
    }

}